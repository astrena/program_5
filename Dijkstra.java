import java.util.*;
import java.io.*;

public class Dijkstra
{
	public static void main (String[] args)
	{
		File file = null;
		Scanner fileScan = null;
		Scanner scan = new Scanner(System.in);

		System.out.println("Enter your input file name.");		// Get file

		try 
		{
			file = new File(scan.next());

			while (!file.exists())
			{
				System.out.println("File does not exist.");
				System.out.println("Re-enter file name: ");
				file = new File(scan.next());
			}

			fileScan = new Scanner(file);
		}

		catch (FileNotFoundException ex) 
		{
			ex.printStackTrace();
		}

		String input = fileScan.nextLine();						// Get input from file
		String[] tokens = input.split("\\s+");
		String[][] matrix = new String[tokens.length][tokens.length];
		matrix[0] = tokens;

		for (int i = 1; fileScan.hasNext(); i++)				// Build adjacency matrix
		{
			input = fileScan.nextLine();
			tokens = input.split("\\s+");
			matrix[i] = tokens;
		}

		System.out.println("Input: \n");
		for (int i = 0; i < matrix.length; i++)
		{
			for (int c = 0; c < matrix[i].length; c++)
			{
				System.out.printf("%3s ", matrix[i][c]);
			}
			System.out.println();
		}
		System.out.println("\n");

		ArrayList<Vertex> Q = new ArrayList<Vertex>();			// Create and fill list of unchecked vertices
		Q.add(new Vertex(0, 0));

		for (int i = 1; i < matrix.length; i++)
		{
			Q.add(new Vertex(i, Integer.MAX_VALUE));
		}

		int min = Integer.MAX_VALUE;							// Declare variables to be used in the loop
		int index = 0;
		Vertex vertex = null;
		Vertex endVertex = null;
		ArrayList<Vertex> checked = new ArrayList<Vertex>();
		int[][] answer = new int[matrix.length][matrix.length];
		int row = 0;

////////////////////////////////// Begin algorithm ///////////////////////////////////////
		for (int c = 0; c < matrix.length; c++) // Once for each vertex
		{
			min = Integer.MAX_VALUE;
			index = 0;
			for (int i = 0; i < Q.size(); i++)	// Get min from unchecked vertices
			{
				if (Q.get(i) != null && Q.get(i).getWeight() < min)
				{
					min = Q.get(i).getWeight();
					index = i;
				}
			}
			
			checked.add(Q.get(index));		// Put it in the list of checked vertices
			Q.set(index, null);
			vertex = checked.get(checked.size()-1);
			row = vertex.getNum();

			// row corresponds to current vertex, column corresponds to end vertex.
			for (int col = 0; col < matrix[row].length; col++)		// For each column in the row
			{
				if (matrix[row][col].equals("inf")) { continue; }
				if (col == row) { continue; }
				// If current vertex weight + edge weight < end vertex weight
				if (Q.get(col) != null && vertex.getWeight() + Integer.parseInt(matrix[row][col]) < Q.get(col).getWeight())
				{
					endVertex = Q.get(col);
					// Change weight of end vertex to current vertex weight + edge weight
					endVertex.setWeight(vertex.getWeight() + Integer.parseInt(matrix[row][col]));

					// Check this column for all vertices in the checked list, and make all but the last 0,
					//																 skipping the case where row == col.
					int rowToCheck = 0;
					for (int i = 0; i < (checked.size() - 1); i++)
					{
						rowToCheck = checked.get(i).getNum();
						if (rowToCheck == col) { continue; }
						else { answer[rowToCheck][col] = 0; }
					}
					answer[row][col] = Integer.parseInt(matrix[row][col]);

					// Change answer[col][col] to new vertex weight.
					answer[col][col] = endVertex.getWeight();
				}
			}
		}
//////////////////////////////////// End algorithm //////////////////////////////////////////

		System.out.println("Output: \n");
		for (int i = 0; i < answer.length; i++)						// Print answer
		{
			for (int c = 0; c < answer[i].length; c++)
			{
				System.out.printf("%3d ", answer[i][c]);
			}
			System.out.println();
		}
		System.out.println();
	}

	private static class Vertex
	{
		int number;
		int weight;

		public Vertex (int num, int w)
		{
			number = num;
			weight = w;
		}

		public void setWeight (int w)
		{
			weight = w;
		}

		public int getWeight ()
		{
			return weight;
		}

		public int getNum ()
		{
			return number;
		}
	}
}