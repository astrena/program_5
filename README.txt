Programming assignment 5
Jenna Coons
CS 4343
4/27/2018

You will be prompted to enter your file name when you run the program. Everything should work as described in the assignment instructions. Having whitespace at the beginning of lines in the file may cause issues.